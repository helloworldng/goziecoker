<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<head>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
  <title>Coker Creative</title>

  <link rel="stylesheet" href="css/foundation.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/animate.css">
  <link rel="stylesheet" href="css/slick.css">
  
</head>
<body>

	<div class="header">
		<div class="logo">
			<img src="img/logo_white.png" alt="">
		</div>

		<div class="menu">
			<ul>
				<li><a href="about.html">About Us</a></li>
				<li><a href="http://cokercreative.tumblr.com">Experiences</a></li>
				<li><a href="contact.html">Get in touch</a></li>
			</ul>
		</div>
	</div>

	<div class="bg-overlay vignette"></div>

	<div class="slider">

						<?php 
						$apikey = "W1gAlEQVLprjPtKTJVJ7CY2QS8NscCaVcJck0KpNGMSJ1t5j2R";
    					$limit = 3;
    					$tumblr = "cokercreative.tumblr.com";
						$apidata = json_decode(file_get_contents("http://api.tumblr.com/v2/blog/$tumblr/posts?api_key=$apikey&limit=$limit"));
						$mypostsdata = $apidata->response->posts;
    					$myposts = array();
					    $i = 0;

				    	foreach($mypostsdata as $postdata) {
				        $post['id'] = $postdata->id;
				        $post['type'] = $postdata->type;
				        $post['date'] = $postdata->date;
				        $post['short_url'] = $postdata->short_url;
				        $post['caption'] = $postdata->caption;
				        $post["photo_url"] = $postdata->photos[0]->original_size->url;
				        $post["width"] = $postdata->photos[0]->original_size->width;
				        $post["height"] = $postdata->photos[0]->original_size->height;
				        $shortcap = substr($post['caption'], 0, 50);
				    
				        ?>
				    	<div class="slide" style="background-image: url('<? echo $post["photo_url"] ?>')"></div>


				    	<?
						$myposts[$i] = $post;
						$i++;
						}
					?>

					<!--div class='post large-6 columns'>
							<div class='img-wrap'>
								<a href='".$post['short_url']."' target='_blank'><img src='".$post["photo_url"]."' /></a>
							</div>
							<a class='title'>".$shortcap."... </a>
						</div-->
		
	</div>

	<div class="tag">
		<!-- <p class="company-name">Coker Creative</p> -->
		WE CREATE EXPERENCES
		<!-- <a href="mailto:gozie@cokercreative.com" class="email-us"><i class="fa fa-envelope-o"></i> Email Us</a> -->

		<div class="box">
			<div class="slider-details">
			<? 
			$k = 0;
			foreach($mypostsdata as $postdata){ 
				$pic['caption'] = $postdata->caption; 
				$pic['short_url'] = $postdata->short_url;  ?>
				<div>
					<p><? echo $pic["caption"] ?></p>
					<a href="<? echo $pic["short_url"] ?>"><i class="fa fa-angle-right"></i> EXPLORE</a>
				</div>
			<?
			$myposts[$k] = $pic;
			$k++;
			}
 ?>
			</div>
		</div>
	</div>

	

	<script src="js/vendor/jquery.js"></script>
	<script src="js/vendor/slick.js"></script>
	<script>
		$(document).ready(function() {

			$('.slider').slick({
				arrows : false,
				dots : false,
				pauseOnHover : false,
				autoplay : true,
				fade : true,
				asNavFor : '.slider-details'
			});

			$('.slider-details').slick({
				arrows : false,
				dots : false,
				pauseOnHover : false,
				autoplay : true,
				vertical: true,
				asNavFor : '.slider'
			});
		});
	</script>

</body>
</html>
